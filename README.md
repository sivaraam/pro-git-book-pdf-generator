# Pro Git Book PDF Generator

## Purpose:
This repository contains a YAML file that will clone the Pro Git Book Repository https://github.com/progit/progit2 and generate a PDF of it for download.

## How to Use:
Under Pipelines, you can download the most recently generated PDF or run the Pipeline to generate a new one. That's it!